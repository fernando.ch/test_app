import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import plotly.graph_objects as go
import plotly.express as px

from datetime import datetime
from matplotlib.dates import DateFormatter

import streamlit as st

import time

st.set_page_config(
     page_title="AAM App",
     page_icon=":sunny:",
     initial_sidebar_state="expanded",
     layout="centered",
 )

#markdown cheatsheet
st.title('AAM NIGERIA')


#Set Merge Date
#Today
merge_date=time.strftime("%Y_%m_%d_")



#specific day (use this format)
#merge_date='2020_12_26_'

st.write('Data merge_date : ' + merge_date)

#Set System Name
aam_name= st.sidebar.selectbox('Select AMM:', ['200','272','346','806'])

#Set Path for merges 
path2merges= "C:/Users/solar/A2EI/SKGS/SKGS_P2_Logs/"+ aam_name+"/merges"
 
#set the day to plot
time_start, time_end = st.beta_columns(2)

time_start = time_start.date_input('start date',value=(datetime(2021, 1, 1)))
time_end = time_end.date_input('end date')


#time_start = st.date_input('start date')
#time_end =st.date_input('end date')

#st.write("Dates:", time_start , time_end)

#time_start = ('2020-12-29 00:00:00 ' )
#time_end = ('2021-02-03 00:00:00' )

#-------------------------------------------------LOG MERGE---------------------------------------------------------
if st.sidebar.button('Sync and merge Logs AAM '+ aam_name):
    #set date
    today= time.strftime("%Y_%m_%d_")

    #Set System Name
    #aam_name='806'

    #set path to logs
    path2logs = "C:/Users/solar/A2EI/SKGS/SKGS_P2_Logs/"+ aam_name+"/logs"

    #create logs folder if doesnt exist
    if not os.path.exists(path2logs):
        os.makedirs(path2logs)

    #set path to merges
    path2merges = "C:/Users/solar/A2EI/SKGS/SKGS_P2_Logs/"+ aam_name+"/merges"

    #create merges folder if doesnt exist
    if not os.path.exists(path2merges):
        os.makedirs(path2merges)


    #Synchronise missing Logs from AWS

    if not os.path.exists(path2merges+"/"+today+"merged_logs_"+ aam_name+".csv"):
        os.system('cmd /c aws s3 sync s3://a2ei-skgsp2-logs/'+ aam_name + ' C:/Users/solar/A2EI/SKGS/SKGS_P2_Logs/' + aam_name+"/logs")

    #set date from where the merge start
    merge_start='01-01-2021 00:00:00'
    pattern = '%d-%m-%Y %H:%M:%S'
    epoch = int(time.mktime(time.strptime(merge_start, pattern)))
    #print(epoch)


    
    #log_header function defines the header of the log files (which columns represents which information)
    
    def log_header():
        header = [  #timestamp
                     'UTC',

                    #inverter

                    'input_voltage',          	# Netzspannung in V
                    'input_fault_voltage',		# ? Netzspannung in V (Unterschied zu input_voltage??)
                    'output_voltage',			# if input voltage = 0 and output voltage 230 V -> Batteriebetrieb
                    'output_current', 			# prozentualer Strom(aber auf 600 normiert --- /6 nötig)
                    'output_frequency',			# Ausgangsfrequenz
                    'battery_voltage',			# Batteriespannung auf 12V-Niveau (x2 nötig)
                    'temperature',				# Temperatur
                    'status',					# 7		1 : Utility Fail (Immediate)
                                                # 6		1 : Battery Low
                                                # 5		1 : AVR 0: NORMAL
                                                # 4		1 : UPS Failed
                                                # 3		1 : UPS Type is Line-Interactive (0 is On_line)
                                                # 2		1 : Test in Progress
                                                # 1		1 : Shutdown Active
                                                # 0		1 : Beeper On

                    #mppt

                    'charging_input_voltage',       # PV voltage
                    'charging_input_current',       # PV current
                    'charging_output_voltage',      # = battery voltage
                    'charging_output_current',      # MPPT charging current
                    'discharging_output_voltage',   
                    'discharging_output_current',   
                    'temperature_inside',
                    'temperature_power_components',
                    'temperature_battery',
                    'soc',
                    'battery_status',			# D15: 1-Wrong identification for rated voltage 
                                                # D8: Battery inner resistance abnormal 1, normal 0 
                                                # D7-D4: 00H Normal, 01H Over Temp.(Higher than the warning settings), 02H Low Temp.(Lower than the warning settings), 
                                                # D3-D0: 00H Normal ,01H Over Voltage. , 02H Under Voltage, 03H Over discharge, 04H Fault
                    'charging_status', 			# D15-D14: Input voltage status. 00H normal, 01H No input power connected, 02H Higher input voltage , 03H Input voltage error.
                                                # D13: Charging MOSFET is short circuit.
                                                # D12: Charging or Anti-reverse MOSFET is open circuit.
                                                # D11: Anti-reverse MOSFET is short circuit.
                                                # D10: Input is over current.
                                                # D9: The load is over current.
                                                # D8: The load is short circuit.
                                                # D7: Load MOSFET is short circuit.
                                                # D6：Disequilibrium in three circuits.
                                                # D4: PV input is short circuit.
                                                # D3-D2: Charging status. 00H No charging,01H Float,02H Boost, 03H Equalization.
                                                # D1: 0 Normal, 1 Fault.
                                                # D0: 1 Running, 0 Standby.

                    #adc

                    'peak_voltage'				# Trafo Übersetzung 230:6, Spannungsteil ?10:3?

                  ]
        return header

   
    #the merge_logs function merges all available small log files from the defined path ('path2logs')
    #to get one big dataframe/csv-file
    

    def merge_logs(path):
        files = os.listdir(path)
        merged_logs = pd.DataFrame(columns=log_header())
        for file in files:
            temp_log = pd.read_csv(path + '/' + file, names=(np.arange(0,22)), index_col=0, engine='python')
            for log_entry in temp_log.itertuples():
                filled_entry = fill_log_entry(log_entry)
                merged_logs = merged_logs.append(filled_entry)

        merged_logs_new_index = merged_logs.set_index(pd.to_datetime(merged_logs.UTC,unit='s'))
        return merged_logs_new_index


    # In[7]:


    
    #if for example inverter communication fails, 'None' is only sent once, instead of 8x to save data traffic
    #therefore the columns need to be filled accordingly
    #This is done with the fill_log_entry function
    

    def fill_log_entry(log_entry):
        filled_entry = np.empty(22)


        # skip entry if UTC timestamp is not available
        if log_entry[0] == 'None' or log_entry[0]<= epoch: 
            return

         # inverter AND MPPT didn't send data    
        if log_entry[1] == 'None' and log_entry[2] == 'None':
            # inverter AND MPPT didn't send data
            for idx,val in enumerate(filled_entry):
                if idx == 0:
                    filled_entry[idx]=float(log_entry[idx])
                if idx >= 1 and idx <= 8:
                    filled_entry[idx] = None
                if idx >= 9 and idx <= 20:
                    filled_entry[idx] = None
                if idx == 21:
                    filled_entry[idx] = float(log_entry[3])

        # inverter didn't send data
        elif log_entry[1] == 'None':
            for idx,val in enumerate(filled_entry):
                if idx == 0:
                    filled_entry[idx]=float(log_entry[idx])
                if idx >= 1 and idx <= 8:
                    filled_entry[idx] = None
                if idx >= 9 and idx <= 21:
                    filled_entry[idx] = float(log_entry[idx-7])

        # MPPT didn't send data

        elif log_entry[9] == 'None':    
            for idx,val in enumerate(filled_entry):
                if idx == 0:
                    filled_entry[idx]=float(log_entry[idx])
                if idx >= 1 and idx <= 8:
                    filled_entry[idx] = float(log_entry[idx])
                if idx >= 9 and idx <= 20:
                    filled_entry[idx] = None
                if idx == 21:
                    filled_entry[idx] = float(log_entry[10])

        else:
            filled_entry = log_entry

        filled_entry = pd.DataFrame(filled_entry).T
        filled_entry.columns = log_header()
        return filled_entry

    #main

    if os.path.exists(path2merges+"/"+today+"merged_logs_"+ aam_name+".csv"):

        #aam_data = merge_logs(path2logs)
        #write CSV file
        #aam_data.to_csv(path2merges+"/"+today+"merged_logs_"+ aam_name+".csv")

        st.warning("Today's merge already exist")
        

    else:
        aam_data = merge_logs(path2logs)
        #write CSV file
        aam_data.to_csv(path2merges+"/"+today+"merged_logs_"+ aam_name+".csv")

        st.warning("Today's merge ready")
        

#-------------------------------------------------------------------END MERGE--------------------------------------------------

#-------------------------------------------------------------------Delete Merge--------------------------------------------------
if st.sidebar.button('Delete today merge'):
    if os.path.exists(path2merges+"/"+merge_date+"merged_logs_"+ aam_name+".csv"):
        os.remove(path2merges+"/"+merge_date+"merged_logs_"+ aam_name+".csv")
    else:
        st.warning("Today's merge doesn't exist")
        st.stop()

#-------------------------------------------------------------------END delete Merge--------------------------------------------------


if not os.path.exists(path2merges+"/"+merge_date+"merged_logs_"+ aam_name+".csv"):
    st.warning('Please sync logs')
    st.stop()

#get all data 
system_data_all = pd.read_csv(path2merges+"/"+merge_date+"merged_logs_"+ aam_name+".csv",
                        parse_dates=['UTC'],
                        index_col=['UTC'])#set Timestamp column as index

#get specific data
system_data= pd.DataFrame(system_data_all)


#--------------------------------------------------------------------
system_data_time = system_data[time_start : time_end]

# Create figure and plot space
#fig, ax = plt.subplots()
fig, ax = plt.subplots(figsize=(9,5))

# Add x-axis and y-axis
#ax.plot(system_data_time.index.values,
 #           system_data_time['battery_voltage']*2,
  #          color='purple',
   #         label='inverter')
# Add x-axis and y-axis
ax.plot(system_data_time.index.values,
            system_data_time['charging_output_voltage'],
            color='blue',
           label='mppt (charging_output_voltage)')

ax2 = ax.twinx()
ax2.plot(system_data_time.index.values,
        system_data_time['output_current']*120,#*24/6/100*3000,
        color='purple',
       label='Output Power')

# Set title and labels for axes
ax.set(xlabel="Date",
       ylabel="Voltage",
       title="Battery Voltage")

# Set title and labels for axes
ax2.set(ylabel="Load_P in W")


# Define the date format
date_form = DateFormatter("%d.%m h:%H")
ax.xaxis.set_major_formatter(date_form)

axes = plt.gca()
#axes.set_xlim([xmin,xmax])
ax.set_ylim([20,30])
ax2.set_ylim([0,600])

ax.legend(loc='best')

plt.grid(b=True, which='major', color='#999999', linestyle='-')

size=10
params = {'legend.fontsize': size,
          #'figure.figsize': (15,8),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.rcParams.update(params)

#plt.xticks([])
#plt.yticks([])

plotlib_cb=st.sidebar.checkbox('Matplotlib Fix') 

if plotlib_cb:
    
    st.pyplot(fig)

plotly_cb=st.sidebar.checkbox('Plotly Interactive') 

if plotly_cb:
    
    st.plotly_chart(fig, use_container_width=True)
    

#st.line_chart(system_data_time.charging_output_voltage)
#st.line_chart(system_data.battery_voltage*2)

#---------------------------------------------------------------------

fig = go.Figure()
fig.add_trace(go.Scatter(x=system_data_time.index.values, 
                         y=system_data_time['charging_output_voltage'], name="linear",
                        line_shape='linear'))

fig.update_traces( mode='lines')

fig.update_layout(
    xaxis=dict(
        showline=True,
        showgrid=True,
        showticklabels=True,
        linewidth=2,
        ticks='outside',
        tickfont=dict(
            family='Arial',
            size=12,
            color='rgb(82, 82, 82)',
        ),
    ),
    yaxis=dict(
        showgrid=True,
        zeroline=True,
        showline=True,
        showticklabels=True,
    ),
    autosize=True,
    margin=dict(
        autoexpand=False,
        l=100,
        r=20,
        t=110,
    ),
    showlegend=False,
    plot_bgcolor='white'
)

st.plotly_chart(fig, use_container_width=True)
#---------------------------------------------------------------------

fig = px.area(system_data_time['charging_output_voltage'],template='seaborn')

st.plotly_chart(fig, use_container_width=True)#

#--------------------------
fig = px.line(system_data_time['charging_output_voltage'],template='seaborn')

fig.add_hline(y=25, 
              annotation_text="Jan 1, 2018 baseline",
              fillcolor="green",
              annotation_position="bottom right",
             line_width=3)

fig.add_vrect(x0="2021-01-27", x1="2021-01-28", col=1,
              annotation_text="decline", 
              annotation_position="top left",
              fillcolor="green", 
              opacity=0.25, 
              line_width=0)

st.plotly_chart(fig, use_container_width=True)#

#-----------------------------------------------------

system_data_time['PV_W']= system_data_time['charging_input_voltage']*system_data_time['charging_input_current']
system_data_time['output_current'] = system_data_time['output_current']*24/6/100*1000

fig = px.line( system_data_time, x=system_data_time.index, y=system_data_time.columns, template='seaborn',  title="PV Power")

fig.update_layout(
    xaxis=dict(
        showline=True,
        showgrid=True,
        showticklabels=True,
        linewidth=2,
        ticks='outside',
        gridcolor='Grey',
        linecolor='Grey',
        mirror=True,
        tickformat="%d %H:%M",
        tickfont=dict(
            family='Arial',
            size=12,
            color='rgb(82, 82, 82)',
        ),
    ),
    yaxis=dict(
        showgrid=True,
        zeroline=True,
        showline=True,
        linecolor='Grey',
        mirror=True,
        showticklabels=True,
        gridcolor='Gray'
    ),
    legend=dict(
        orientation="h",
        yanchor="bottom",
        y=1.02,
        xanchor="right",
        x=1),
    autosize=True,
    margin=dict(
        autoexpand=True,
        l=100,
        r=20,
        t=110,
    ),
    showlegend=True,
    plot_bgcolor='white'
    
)

st.plotly_chart(fig)

#---------------------------------------
PV_W= system_data_time['charging_input_voltage']*system_data_time['charging_input_current']

fig = go.Figure()

fig.add_trace(go.Scatter(
    x=system_data_time.index,
    y=PV_W,
    mode="lines",
    name="Lines, Markers and Text",
    text=["Text A", "Text B", "Text C"],
    textposition="top center"))


st.plotly_chart(fig)